/**
 * Bootstrap
 * (sails.config.bootstrap)
 *
 * An asynchronous bootstrap function that runs before your Sails app gets lifted.
 * This gives you an opportunity to set up your data model, run jobs, or perform some special logic.
 *
 * For more information on bootstrapping your app, check out:
 * https://sailsjs.com/config/bootstrap
 */

module.exports.bootstrap = function(done) {

	Vaga.createEach([
    {
      titulo:'Analista de Testes com inglês',
      descricao: "<b>Requisitos</b>: <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent sed felis sollicitudin, gravida dolor maximus, laoreet justo. Sed ut malesuada ante, in facilisis leo. Etiam tincidunt libero in nibh sodales luctus. Maecenas lobortis dapibus arcu, non dictum turpis ornare id. Aliquam id ante nisi. Aliquam erat volutpat. Pellentesque non nunc dignissim nisl eleifend fringilla.</p>"
    },
    {
      titulo:'Analista de Testes'
    },
    {
      titulo:'Analista de Testes Automatizados'
    },
    {
      titulo:'Desenvolvedor de Automação com inglês'
    },
    {
      titulo:'Desenvolvedor Java com inglês avançado'
    },
    {
      titulo:'Desenvolvedor Java'
    },
    {
      titulo:'Arquiteto Java'
    },
    {
      titulo:'Desenvolvedor .NET Sênior com inglês'
    },
    {
      titulo:'Desenvolvedor C# Xamarin com inglês'
    },
    {
      titulo:'DevOps'
    },
    {
      titulo:'Desenvolvedor Angular com inglês'
    },
    {
      titulo:'Desenvolvedor Android'
    },
    {
      titulo:'Desenvolvedor iOS'
    },
    {
      titulo:'Desenvolvedor Full Stack'
    },
    {
      titulo:'Líder técnico Mobile'
    },
    {
      titulo:'Gerente de Projetos com inglês'
    }
  ]).then(function (result){
  	done();
  }).catch(function(err){
    done();
  });
  // It's very important to trigger this callback method when you are finished
  // with the bootstrap!  (otherwise your server will never lift, since it's waiting on the bootstrap)
  //return done();

};
