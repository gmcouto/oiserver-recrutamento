/**
 * CandidatoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
	apply: function(req,res) {
		if(!req.param('nome'))
			return res.badRequest('Nome não pode estar vazio.');
		if(!req.param('vaga'))
			return res.badRequest('Vaga não pode estar vazio.');
		if(!req.param('email'))
			return res.badRequest('Email não pode estar vazio.');
		if(!req.file('curriculum'))
			return res.badRequest('Currículo não pode estar vazio.');

		req.file('curriculum').upload(function(err, uploadedFiles){
			if (err) {
				return res.negotiate(err);
			}
			if (uploadedFiles.length === 0){
				return res.badRequest('Nenhum arquivo enviado.');
			}
			EmailService.sendCurriculum(uploadedFiles[0], req.allParams());
			res.ok();
		});
	}
};

