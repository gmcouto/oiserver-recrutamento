const nodemailer = require('nodemailer');
module.exports = {
	sendCurriculum: function(fileinfo, params) {
	    // create reusable transporter object using the default SMTP transport
	    var password = process.env.SMTP_PASSWORD || sails.config.smtp.password;
		let transporter = nodemailer.createTransport({
		    host: 'smtp.gmail.com',
		    port: 465,
		    secure: true, // secure:true for port 465, secure:false for port 587
		    auth: {
		        user: 'gmcouto.1@gmail.com',
		        pass: password
		    }
		});

		// setup email data with unicode symbols
		let mailOptions = {
		    from: '"OiServer Recrutamento 👻" <gmcouto.1@gmail.com>', // sender address
		    to: 'gmcouto@gmail.com', // list of receivers
		    subject: params.vaga+' ✔', // Subject line
		    text: 'O candidato '+params.nome+'('+params.email+') se candidatou para a vaga '+params.vaga+'. Curriculo anexo.', // plain text body
		    html: 'O candidato '+params.nome+'('+params.email+') se candidatou para a vaga '+params.vaga+'. Curriculo anexo.', // html body,
		    attachments: [{filename:fileinfo.filename,path:fileinfo.fd}]
		};

		// send mail with defined transport object
		transporter.sendMail(mailOptions, (error, info) => {
		    if (error) {
		        return console.log(error);
		    }
		    console.log('Message %s sent: %s', info.messageId, info.response);
		});
	}
}