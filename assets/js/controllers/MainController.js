angular.module('recrutamentoApp').controller('MainController', ['$scope', '$location', '$http', '$sce', function MainController($scope, $location, $http, $sce) {
	var objectToForm = function(object) {
		var form = new FormData();
		for(key in object) {
		  if (object[key])
		    form.append(key, object[key]);
		}
		return form;
	}

	$scope.openForm = function(vaga) {
		$scope.candidato.vaga = vaga;
		$location.path('/metodo1');
	}

	$scope.candidato = {
	};

	$scope.asTrusted = function(text) {
	return $sce.trustAsHtml(text)
	}

	$scope.envia = function() {
	var form = objectToForm($scope.candidato);
	$http({
	  method: 'POST',
	  url: '/apply',
	  data: form,
	  headers: {'Content-Type': undefined}
	}).then(function(resp){
	  console.log(resp);
	}).catch(function(error){
	  console.log(error);
	});
	};
	$http.get('/vaga').then(function(resp){console.log(resp); $scope.vagas = resp.data});
	$scope.isActive = function (viewLocation) { 
	    return viewLocation === $location.path();
	};
}]);