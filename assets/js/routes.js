 angular.module('recrutamentoApp').config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');
      $routeProvider.
        when('/metodo1', {
          templateUrl: '/templates/metodo1.html'
        }).
        when('/metodo2', {
          templateUrl: '/templates/metodo2.html'
        }).
        otherwise('/metodo1');
    }
  ]);