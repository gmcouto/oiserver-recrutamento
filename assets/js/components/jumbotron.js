angular.module('recrutamentoApp').component('jumbotron', {
  templateUrl: '/templates/jumbotron.html',
  bindings: {
  	titulo: '@',
  	subtitulo: '@',
  }
});